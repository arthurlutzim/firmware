# 0x33.board firmware

See [the documentation][docs] for information on how to use the firmware.
The rest of this documentation contains information for firmware development.

## Installation
1. Make sure your board is in `dev_mode` (see below)
2. Copy all files from this repository to the CircuitPython drive
3. Install the dependencies:
   - using [circup][circup]: `$ circup [--path /path/to/drive] install -r requirements.txt`
   - manually download the needed libraries from the [Adafruit CircuitPython Library Bundle][cpy-lib-bundle]
     and place them in the `lib` folder
4. Set hardware settings in `config.py`:  
   Import the module from `hex33board.boards` corresponding to the hardware revision you have.

### Simulator
The simulator can be run directly in the repo and using CPython.

1. (optionally) create and enter a venv  
   `$ python -m venv --system-site-packages venv`  
   `$ source venv/bin/activate`  
   The `--system-site-packages` option allows you to use your distribution's Pillow packages.
2. install the dependencies: `$ pip install -r requirements.sim.txt`
3. run the simulator: `$ python simulator.py`
4. open the web interface ([http://localhost:8000](http://localhost:8000))

## Boot modes
After a reset, `boot.py` is executed. Depending on the `dev_mode` value in `config.py` and
whether the menu button (very top left of the board) is pressed, the board boots in different modes:

With `dev_mode = False` and the button not pressed, the board starts in "normal mode".
The board will show up as a read-only flash drive on the computer,
and settings changed in the boards menu will be saved automatically.

When the button is pressed, the board starts in "dev mode".
In this case the board shows up read-write on the computer and will autoreload as the firmware is changed.
While "dev mode" is active, settings changed on the board aren't saved.

If `dev_mode = True` in `config.py`, things work the other way around:
the board goes into "dev mode" by default, but will start in "normal mode" when the menu key is held during reset.

## Using the Simulator

[docs]: https://s-ol.nu/0x33.board/doc
[circup]: https://github.com/adafruit/circup
[cpy-lib-bundle]: https://circuitpython.org/libraries
