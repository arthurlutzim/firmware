import supervisor
import digitalio
import storage
import config as board

try:
    supervisor.set_usb_identification("s-ol", "0x33.board", 0x1209, 0x2609)
except AttributeError:
    pass

col = digitalio.DigitalInOut(board.matrix_pins[0][0])
row = digitalio.DigitalInOut(board.matrix_pins[1][-1])
col.switch_to_output(value=True)
row.switch_to_input(pull=digitalio.Pull.DOWN)
mode_pressed = row.value

root = storage.getmount("/")

if mode_pressed == board.dev_mode:
    storage.disable_usb_drive()
    storage.remount("/", False)
    print("Mounting read-write")

    if root.label == "" or root.label == "CIRCUITPY":
        root.label = "hex33board"
else:
    print("Mounting readonly")
