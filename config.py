from hex33board.boards.r4_5e4bf5c import *


''' boot in dev_mode by default

When active, the firmware drive will be writable on boot.
'''
dev_mode = False

''' forward Key events via USB MIDI

This can be useful to control a simulator instance running on a computer
using the physical buttons on a hardware unit.
'''
sysex_key_sync = False
