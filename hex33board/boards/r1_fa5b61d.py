from __future__ import annotations

import board
import usb_midi
from busio import I2C, UART
from audiopwmio import PWMAudioOut
from digitalio import DigitalInOut, Pull
from displayio import I2CDisplay
from neopixel import NeoPixel
from adafruit_midi import MIDI
from adafruit_displayio_ssd1306 import SSD1306

from ..matrix import mapping_left_right
from .. import Keyboard


matrix_pins = (
    [board.GP8, board.GP4, board.GP0, board.GP6, board.GP7, board.GP9],
    [board.GP5, board.GP1, board.GP2, board.GP3, board.GP10],
)

pixels_pin = board.GP11
display_pins = {"sda": board.GP14, "scl": board.GP15}

midi_pins = {"tx": board.GP16, "rx": None}
audio_pins = {"left_channel": board.GP12, "right_channel": board.GP13}


class Matrix:
    """
    A Scanner for Keyboard Matrices with Diodes in both directions.

    In a bidirectional matrix, each (col, row) crossing can be used twice -
    once with a ROW2COL diode ("A"), and once with a COL2ROW diode ("B").

    The raw key numbers returned by this scanner are based on this layout ("up_down"):

        C1  C2  C3
      +-----------
    R1| A0  A1  A2
    R2| A3  A4  A5
      +-----------
    R1| B6  B7  B8
    R1| B9 B10 B11

    If the physical layout of the matrix is different, you can pass a function
    for `mapping`. The function is passed `len_cols` and `len_rows` and should
    return a `coord_mapping` list.
    Various common mappings are provided in this module, see:
    - `kmk.scanners.bidirectional.mapping_left_right`
    - `kmk.scanners.bidirectional.mapping_left_right_mirrored`
    - `kmk.scanners.bidirectional.mapping_up_down`
    - `kmk.scanners.bidirectional.mapping_up_down_mirrored`
    - `kmk.scanners.bidirectional.mapping_interleave_rows`
    - `kmk.scanners.bidirectional.mapping_interleave_cols`

    :param cols: A sequence of pins that are the columns for matrix A.
    :param rows: A sequence of pins that are the rows for matrix A.
    :param mapping: A coord_mapping generator function, see above.
    """

    def __init__(self, cols, rows, mapping=mapping_left_right):
        self.len_cols = len(cols)
        self.len_rows = len(rows)
        self.half_size = self.len_cols * self.len_rows
        self.keys = self.half_size * 2

        self.coord_mapping = mapping(self.len_cols, self.len_rows)

        # A pin cannot be both a row and column, detect this by combining the
        # two tuples into a set and validating that the length did not drop
        #
        # repr() hackery is because CircuitPython Pin objects are not hashable
        unique_pins = {repr(c) for c in cols} | {repr(r) for r in rows}
        assert (
            len(unique_pins) == self.len_cols + self.len_rows
        ), "Cannot use a pin as both a column and row"
        del unique_pins

        # __class__.__name__ is used instead of isinstance as the MCP230xx lib
        # does not use the digitalio.DigitalInOut, but rather a self defined one:
        # https://github.com/adafruit/Adafruit_CircuitPython_MCP230xx/blob/3f04abbd65ba5fa938fcb04b99e92ae48a8c9406/adafruit_mcp230xx/digital_inout.py#L33

        self.cols = [
            x if x.__class__.__name__ == "DigitalInOut" else DigitalInOut(x)
            for x in cols
        ]
        self.rows = [
            x if x.__class__.__name__ == "DigitalInOut" else DigitalInOut(x)
            for x in rows
        ]

        self.state = bytearray(self.keys)

    def scan_for_changes(self):
        for (inputs, outputs, flip) in [
            (self.rows, self.cols, False),
            (self.cols, self.rows, True),
        ]:
            for pin in outputs:
                pin.switch_to_input()

            for pin in inputs:
                pin.switch_to_input(pull=Pull.DOWN)

            for oidx, opin in enumerate(outputs):
                opin.switch_to_output(value=True)

                for iidx, ipin in enumerate(inputs):
                    if flip:
                        ba_idx = oidx * len(inputs) + iidx + self.half_size
                    else:
                        ba_idx = iidx * len(outputs) + oidx

                    # cast to int to avoid
                    #
                    # >>> xyz = bytearray(3)
                    # >>> xyz[2] = True
                    # Traceback (most recent call last):
                    #   File "<stdin>", line 1, in <module>
                    # OverflowError: value would overflow a 1 byte buffer
                    #
                    # I haven't dived too far into what causes this, but it's
                    # almost certainly because bool types in Python aren't just
                    # aliases to int values, but are proper pseudo-types
                    new_val = int(ipin.value)
                    old_val = self.state[ba_idx]

                    if old_val != new_val:
                        self.state[ba_idx] = new_val
                        yield self.coord_mapping.index(ba_idx), new_val

                opin.switch_to_input()


def create_matrix(board):
    return Matrix(*matrix_pins)


def create_pixels(board, **kwargs):
    return NeoPixel(pixels_pin, 48 + 4, **kwargs)


def create_display(board, **kwargs):
    i2c = I2C(**display_pins, frequency=1000000)
    bus = I2CDisplay(i2c, device_address=0x3C)
    return SSD1306(bus, rotation=180, **kwargs)


def create_midi_trs(board):
    return MIDI(None, UART(**midi_pins, baudrate=31250))


def create_midi_usb(board):
    midi_in = next(p for p in usb_midi.ports if isinstance(p, usb_midi.PortIn))
    midi_out = next(p for p in usb_midi.ports if isinstance(p, usb_midi.PortOut))
    return MIDI(midi_in, midi_out)


def create_i2c(board, **kwargs):
    return None


def create_i2ctarget(board, **kwargs):
    return None


def create_audio_out(board):
    return PWMAudioOut(**audio_pins)
