import board
import usb_midi
from busio import I2C, UART
from audiopwmio import PWMAudioOut
from displayio import I2CDisplay
from neopixel import NeoPixel
from adafruit_midi import MIDI
from adafruit_displayio_ssd1306 import SSD1306

from ..matrix import Matrix
from .. import Keyboard

matrix_pins = (
    # cols
    [board.GP9, board.GP4, board.GP0, board.GP6, board.GP7, board.GP8],
    # rows
    [
        board.GP13,
        board.GP12,
        board.GP11,
        board.GP10,
        board.GP5,
        board.GP1,
        board.GP2,
        board.GP3,
        board.GP22,
    ],
)

pixels_pin = board.GP17

display_pins = {"sda": board.GP14, "scl": board.GP15}

midi_pins = {"tx": board.GP16, "rx": None}

sda = board.GP20
scl = board.GP21

audio_pins = {"left_channel": board.GP18, "right_channel": board.GP19}


def create_matrix(board):
    return Matrix(*matrix_pins)


def create_pixels(board, **kwargs):
    return NeoPixel(pixels_pin, 48 + 4, **kwargs)


def create_display(board, **kwargs):
    i2c = I2C(**display_pins, frequency=1000000)
    bus = I2CDisplay(i2c, device_address=0x3C)
    return SSD1306(bus, rotation=180, **kwargs)


def create_midi_trs(board):
    return MIDI(None, UART(**midi_pins, baudrate=31250))


def create_midi_usb(board):
    midi_in = next(p for p in usb_midi.ports if isinstance(p, usb_midi.PortIn))
    midi_out = next(p for p in usb_midi.ports if isinstance(p, usb_midi.PortOut))
    return MIDI(midi_in, midi_out)


def create_i2c(board, **kwargs):
    return I2C(sda=sda, scl=scl, **kwargs)


def create_i2ctarget(board, **kwargs):
    from i2ctarget import I2CTarget

    return I2CTarget(sda=sda, scl=scl, **kwargs)


def create_audio_out(board):
    return PWMAudioOut(**audio_pins)
