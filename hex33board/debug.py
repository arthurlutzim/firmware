import time

times = []
names = []


def start():
    times.clear()
    names.clear()
    times.append(time.monotonic_ns())


def step(name):
    times.append(time.monotonic_ns())
    names.append(name)


def stop():
    timestr = ' + '.join(
        f"{(times[i+1] - times[i]) // 1000000}ms {text}" for i, text in enumerate(names)
    )
    print(f"{timestr} = {(times[-1] - times[0]) // 1000000}ms")
