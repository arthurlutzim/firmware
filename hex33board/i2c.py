from __future__ import annotations

from .core import Note

NOTE_ON_BIT = 1 << 7
PITCH_MASK = ~NOTE_ON_BIT


class I2CModule:
    queue: list[int]
    keyboard: Keyboard
    base: BaseMode

    MSG_NOP = bytes([0, 0])

    def __init__(self, keyboard: Keyboard):
        self.queue = []
        self.keyboard = keyboard
        self.base = self.keyboard.modes['base']

    def process_msg(self, msg):
        pitch = msg & PITCH_MASK
        on = msg & NOTE_ON_BIT

        if on:
            Note(self.keyboard, pitch).on(local=False)
        elif pitch in self.base.notes:
            self.base.notes[pitch].off(local=False)

    def send(self, note, on):
        if on:
            self.queue.append(note.pitch | NOTE_ON_BIT)
        else:
            self.queue.append(note.pitch)

class I2CLeader(I2CModule):
    devices: list[int]

    def __init__(self, keyboard: Keyboard, board):
        super().__init__(keyboard)
        self.i2c = board.create_i2c(frequency=1000000, timeout=1000)

        self.i2c.try_lock()
        self.devices = self.i2c.scan()

    def broadcast(self, msg: buffer, except_addr=None):
        for device in self.devices:
            if device == except_addr:
                continue

            self.i2c.writeto(device, msg)

    def tick(self):
        msg = bytearray(2)
        incoming = {}

        for device in self.devices:
            incoming[device] = []

            more = True
            while more:
                self.i2c.readfrom_into(device, msg)

                if not msg[0]:
                    break

                self.process_msg(msg[1])
                self.broadcast(msg[1], device)

                more = msg[0] > 1

        while self.queue:
            self.broadcast(bytes([self.queue.pop(0)]))

    def deinit(self):
        self.i2c.deinit()


class I2CFollower(I2CModule):
    def __init__(self, keyboard: Keyboard, board, address):
        super().__init__(keyboard)
        self.i2c = self.board.create_i2ctarget(addresses=(address,))

    def pop_msg(self):
        if self.queue:
            rest = len(self.queue)
            return bytes([rest, self.queue.pop(0)])
        else:
            return self.MSG_NOP

    def tick(self):
        req = self.i2c.request()
        if not req:
            return

        with req:
            if req.is_read:
                req.write(self.pop_msg())
            else:
                self.process_msg(req.read(1)[0])

    def deinit(self):
        self.i2c.deinit()
