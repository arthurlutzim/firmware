from __future__ import annotations

from .core import Key


class Layout:
    offset: int

    def __init__(self, offset: int = 24):
        self.offset = offset

    def get_pitch(self, key: Key) -> int:
        pass


class WickiHaydenLayout(Layout):
    def get_pitch(self, key: Key) -> int:
        x, y = key.pos
        return int(self.offset + 2 * x + 6 * y)


class HarmonicLayout(Layout):
    def get_pitch(self, key: Key) -> int:
        x, y = key.pos
        return int(self.offset + 4 * x + 5 * y)


class GerhardLayout(Layout):
    def get_pitch(self, key: Key) -> int:
        x, y = key.pos
        return int(self.offset + 3 * x + 2.5 * y)


class JankoLayout(Layout):
    def get_pitch(self, key: Key) -> int:
        x, y = key.pos
        return int(self.offset + 2 * x)


class TradPianoLayout(Layout):
    PITCHES = [0, 1, 2, 3, 4, -1, 5, 6, 7, 8, 9, 10, 11, -1]
    def get_pitch(self, key: Key) -> int:
        x, y = key.pos

        p = self.PITCHES[int(2 * x) % 14]
        o = (2*x) // 14 + y // 2

        if p < 0:
            return p

        return int(self.offset + p + 12 * o)


LAYOUTS = {
    "wicki/hayden": WickiHaydenLayout,
    "harmonic table": HarmonicLayout,
    "gerhard": GerhardLayout,
    "jankó": JankoLayout,
    "trad piano": TradPianoLayout,
}
