from __future__ import annotations

from adafruit_bitmap_font import bitmap_font

FONT_10 = bitmap_font.load_font("fonts/bitbuntu.pcf")

led_map = []
led_map.extend(range(15, 3, -1))
led_map.extend(range(16, 28))
led_map.extend(range(39, 27, -1))
led_map.extend(range(40, 52))
