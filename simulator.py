import hex33board.boards.simulator as board

board.dev_mode = False
board.sysex_key_sync = False

k = board.Keyboard(board)
k.run()
